<?php
    echo $date;
    include("config.php");
    include('session.php');

    $ssh_a_us_path = "ssh_a_us.txt";
    $ssh_a_tw_path = "ssh_a_tw.txt";
    $fp_us_a = array_filter(array_map("trim", file($ssh_a_us_path)), "strlen");
    $fp_tw_b = array_filter(array_map("trim", file($ssh_a_tw_path)), "strlen");
    $ssh_a_us_total = count($fp_us_a);
    $ssh_a_tw_total = count($fp_tw_b);

    $ssh_b_us_path = "ssh_b_us.txt";
    $ssh_b_tw_path = "ssh_b_tw.txt";
    $fp_us_b = array_filter(array_map("trim", file($ssh_b_us_path)), "strlen");
    $fp_tw_b = array_filter(array_map("trim", file($ssh_b_tw_path)), "strlen");
    $ssh_b_us_total = count($fp_us_b);
    $ssh_b_tw_total = count($fp_tw_b);

    $member_sql = "SELECT * FROM user WHERE level=2";
    $result = mysqli_query($conn, $member_sql);
?>

<?php
   include 'modules/header.php';
   include 'modules/country.php';
?>

<?php
  if ($level == 1) { // if as admin
    include 'modules/member.php';
    include 'modules/data.php';
  }
?>

<?php
   include 'modules/footer.php';
?>
