function memberDelete(id) {
  if (confirm("Delete member " + id + ". Are you sure?")) {
    $.get("member_delete.php", {
        id: id
      },
      function(msg) {
        alert(msg);
        $(location).attr('href', 'index.php')
      });
  }
}

$("#memberAddBtn").click(function() {
  username = $("#username").val();
  password = $("#password").val();
  $.post("member_add.php", {
      username: username,
      password: password
    },
    function(msg) {
      alert(msg);
      $(location).attr('href', 'index.php')
    });
});

$(".getAll").click(function() {
  if (confirm("Are you sure?")) {
    $.post("reset.php", {
        btn_id: $(this).attr('id')
      },
      function(msg) {
        $(location).attr('href', 'index.php');
      });
  }
});

$(".reset").click(function() {
  if (confirm("Are you sure?")) {
    $.post("reset.php", {
        btn_id: $(this).attr('id')
      },
      function(msg) {
        $(location).attr('href', 'index.php');
      });
  }
});
