<?php
  include 'config.php';
  session_start();
  if ($_SESSION['level'] != 1) {
    echo "You're NOT an admin!";
    exit;
  }

  if (isset($_GET['id'])) {
    // sql to delete a record
    $sql = "DELETE FROM user WHERE id='".$_GET['id']."'";

    if ($conn->query($sql) === TRUE) {
        echo "Success!";
    } else {
        echo "Error deleting record: " . $conn->error;
    }
  }else {
    echo "Oops!";
  }

?>
