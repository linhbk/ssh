<div class="row">
   <div class="col-md-6">
      <div class="panel panel-primary">
         <div class="panel-heading"><strong>United States (US) <span class="badge"><?= $ssh_b_us_total ?></span></strong></div>
         <div class="panel-body">
            <div class="row">
               <div class="col-md-6">
                 <form class="" action="ssh_get.php" method="post">
                   <input type="number" name="quantityUs" min="1" max="50">
                   <input type="submit" class="btn btn-primary" name="getSshUs" value="Get SSH US">
                 </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-6">
      <div class="panel panel-primary">
         <div class="panel-heading"><strong>Taiwan (TW) <span class="badge"><?= $ssh_b_tw_total ?></span></strong></div>
         <div class="panel-body">
            <div class="row">
               <div class="col-md-6">
                 <form class="" action="ssh_get.php" method="post">
                   <input type="number" name="quantityTw" min="1" max="50">
                   <input type="submit" class="btn btn-primary" name="getSshTw" value="Get SSH TW">
                 </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
