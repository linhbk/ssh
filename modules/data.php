<div class="row">
   <div class="col-md-6">
      <div class="panel panel-primary">
         <div class="panel-heading"><strong>United States (US) A<span class="badge"><?= $ssh_a_us_total ?></span> B<span class="badge"><?= $ssh_b_us_total ?></span></strong></div>
         <div class="panel-body">
            <div class="row">
               <div class="col-md-6">
                    <form class="" action="ssh_get.php" method="post">
                      <input type="submit" class="btn btn-primary" name="getAllUsA" value="A: Get all US">
                    </form>
               </div>
               <div class="col-md-6">
                     <button class="btn btn-danger reset" id="resetUsA">A: Reset US</button>
               </div>
            </div>
            <div class="row top-buffer">
               <div class="col-md-6">
                 <form class="" action="ssh_get.php" method="post">
                   <input type="submit" class="btn btn-primary" name="getAllUsB" value="B: Get all US">
                 </form>
               </div>
               <div class="col-md-6">
                     <button class="btn btn-danger reset" id="resetUsB">B: Reset US</button>
               </div>
            </div>
            <div class="row top-buffer">
               <div class="col-md-6">
                 <form action = "ssh_set.php" method = "POST" enctype = "multipart/form-data">
                   <input type = "file" name = "ssh_us">
                   <input type = "submit" class="btn btn-default" value="Up SSH US">
                 </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-6">
      <div class="panel panel-primary">
         <div class="panel-heading"><strong>Taiwan (TW) A<span class="badge"><?= $ssh_a_tw_total ?></span> B<span class="badge"><?= $ssh_b_tw_total ?></span></strong></div>
         <div class="panel-body">
           <div class="row">
              <div class="col-md-6">
                <form class="" action="ssh_get.php" method="post">
                  <input type="submit" class="btn btn-primary" name="getAllTwA" value="A: Get all TW">
                </form>
              </div>
              <div class="col-md-6">
                    <button class="btn btn-danger reset" id="resetTwA">A: Reset US</button>
              </div>
           </div>
           <div class="row top-buffer">
              <div class="col-md-6">
                <form class="" action="ssh_get.php" method="post">
                  <input type="submit" class="btn btn-primary" name="getAllTwB" value="B: Get all TW">
                </form>
              </div>
              <div class="col-md-6">
                    <button class="btn btn-danger reset" id="resetTwB">B: Reset US</button>
              </div>
           </div>
            <div class="row top-buffer">
               <div class="col-md-6">
                  <form action = "ssh_set.php" method = "POST" enctype = "multipart/form-data">
                     <input type = "file" name = "ssh_tw">
                     <input type = "submit" class="btn btn-default" value="Up SSH TW">
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
