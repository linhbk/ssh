<!DOCTYPE html>
<html>
   <head>
      <title>SSH</title>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <!-- Optional theme -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
      <!-- Latest compiled and minified JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
      <link rel="shortcut icon" href="favicon.ico">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
      <style media="screen">
        .top-buffer { margin-top:20px; }
        .dollar { display: inline; }
        body {
          background: #DADFE1;
        }
      </style>
   </head>
   <body>
      <!-- Page Content -->
      <div class="container">
         <div class="row">
            <div class="col-md-3" style="margin-top: 7.4%;">
               <p class="lead">Hello <strong><em style="color: #00B16A"><?= $username ?></em></strong></p>
               <div class="list-group">
                  <a href="#" class="list-group-item active">Menu</a>
                  <a href="http://getmoremoney.top/managerOffer/login.php" class="list-group-item">My Site</a>
               </div>
               <a class="btn btn-success" href="logout.php" role="button">Sign Out</a>
            </div>
            <div class="col-md-9">
               <div class="page-header">
                  <h1 align="center"><span style="color: #1E824C; font-size: 50px;"><b>$$#</b></span><strong style="color: #F9690E"> ONLINE MANAGEMENT</strong></h1>
               </div>
