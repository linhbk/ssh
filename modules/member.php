<div class="row">
   <div class="col-md-12">
      <div class="panel panel-primary">
         <div class="panel-heading"><strong>Members <span class="badge"><?= mysqli_num_rows($result) ?></span></strong></div>
         <div class="panel-body">
            <table class="table table-striped">
               <thead>
                  <tr>
                     <th>#</th>
                     <th>Username</th>
                     <th>Password</th>
                     <th>Delete</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     if (mysqli_num_rows($result) > 0) {
                         // output data of each row
                         $i = 0;
                         while($row = mysqli_fetch_assoc($result)) {
                             echo "<tr><td>".++$i."</td><td>".$row["username"]."</td><td>".$row["password"]
                             ."</td><td><button type='button' class='btn btn-warning'"
                             ." onclick='memberDelete(".$row['id'].");'>Del</button></td></tr>";
                         }
                     } else {
                         echo "0 results";
                     }
                     ?>
               </tbody>
            </table>
            <div class="form-inline">
               <div class="form-group">
                  <input type="text" id="username" class="form-control" placeholder="Username">
               </div>
               <div class="form-group">
                  <input type="password" id="password" class="form-control" placeholder="Password">
               </div>
               <button class="btn btn-success" id="memberAddBtn">Add member</button>
            </div>
         </div>
      </div>
   </div>
</div>
