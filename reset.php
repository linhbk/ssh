<?php
  include 'constant.php';
  include 'util/admin.php';

  function reset_all($path){
    chmod($path, 0777);
    unlink($path);
    fopen($path, "w");
  }

  switch ($_POST['btn_id']) {
    case 'resetUsA':
      reset_all($sshUsAPath);
      break;
    case 'resetUsB':
      reset_all($sshUsBPath);
      break;
    case 'resetTwA':
      reset_all($sshTwAPath);
      break;
    case 'resetTwB':
      reset_all($sshTwBPath);
      break;
  }
?>
