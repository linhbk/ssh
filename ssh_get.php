<?php
  include "constant.php";
  include 'util/file.php';
  session_start();

  function sendHeaders($file, $type, $name=NULL){
    if (empty($name))
    {
        $name = basename($file);
    }
    header('Pragma: public');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private', false);
    header('Content-Transfer-Encoding: binary');
    header('Content-Disposition: attachment; filename="'.$name.'";');
    header('Content-Type: ' . $type);
    header('Content-Length: ' . filesize($file));
  }

  function get_ssh_limitation($path){
    $f = fopen($path, 'r');
    $line = fgets($f);
    fclose($f);
    return (int)$line;
  }

  function getAllSsh($filePath, $downloadName){
    if (is_file($filePath)){
        sendHeaders($filePath, 'text/plain', $downloadName);
        ob_clean();
        flush();
        @readfile($filePath);
        unlink($filePath);
        fopen($filePath, "w");
        exit;
    }
  }

  function get_ssh($ssh_b_path, $ssh_download_path, $quantity){
    $ssh_b = fopen($ssh_b_path, 'r');
    $i = $quantity;
    if ($ssh_b) {
        while (($ssh_b_line = fgets($ssh_b)) !== false) {
          file_put_contents($ssh_download_path, $ssh_b_line, FILE_APPEND);
          rewind($ssh_b);
          deleteLineInFile($ssh_b_path, $ssh_b_line);
          if ($i-- == 1) {
            break;
          }
        }
    }
    download($ssh_download_path, $_SESSION['username'].'-'.date('Y-m-d h:i:s', time()));
  }

  function get_ssh_test($ssh_b_path, $quantity)
  {
    $new_file_name = "ssh_download.txt";
    $new_file = new SplFileObject($new_file_name, 'w');
    $file = file($ssh_b_path);

    foreach (new LimitIterator(new SplFileObject($ssh_b_path), 0, $quantity) as $line){
      $new_file->fwrite($line);
    }

    for( $i = 0; $i<$quantity; $i++ ) {
        read_and_delete_first_line($ssh_b_path);
    }

    download($new_file_name, $_SESSION['username'].'-'.date('Y-m-d h:i:s', time()).'.txt');
  }

  function read_and_delete_first_line($filename) {
    $file = file($filename);
    $output = $file[0];
    unset($file[0]);
    file_put_contents($filename, $file);
    return $output;
  }

  function download($file, $downloadName){
    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$downloadName.'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        unset($file);
        $myfile = fopen("ssh_download.txt", "w");
        exit;
    }
  }

  function deleteLineInFile($file,$string)
  {
  	$i=0;$array=array();

  	$read = fopen($file, "r") or die("can't open the file");
  	while(!feof($read)) {
  		$array[$i] = fgets($read);
  		++$i;
  	}
  	fclose($read);

  	$write = fopen($file, "w") or die("can't open the file");
  	foreach($array as $a) {
  		if(!strstr($a,$string)) fwrite($write,$a);
  	}
  	fclose($write);
  }

  if (!isset($_SESSION['username'])) {
    print_r("Please login");
    exit;
  }

  $ssh_limit = get_ssh_limitation($ssh_limit_path);
  $source = "";
  $quantity = 0;

  if (isset($_POST["getSshUs"]) && isset($_POST["quantityUs"])) {
      $source = "ssh_b_us.txt";
      $quantity = $_POST["quantityUs"];
  }elseif (isset($_POST["getSshTw"]) && isset($_POST["quantityTw"])) {
      $source = "ssh_b_tw.txt";
      $quantity = $_POST["quantityTw"];
  }

  if (strlen($source) && $quantity > 0 && $quantity <= $ssh_limit) {
    # get_ssh($source, $ssh_download_path, $quantity);
    get_ssh_test($source, $quantity);
    exit;
  }

  if (isset($_SESSION['level']) && $_SESSION['level'] == 1) {
    if (isset($_POST["getAllUsA"])) {
      $downloadName = "us-a-".date('Y-m-d h:i:s', time());
      getAllSsh($sshUsAPath, $downloadName);
      exit;
    }

    if (isset($_POST["getAllUsB"])) {
      $downloadName = "us-b-".date('Y-m-d h:i:s', time());
      getAllSsh($sshUsBPath, $downloadName);
      exit;
    }

    if (isset($_POST["getAllTwA"])) {
      $downloadName = "tw-a-".date('Y-m-d h:i:s', time());
      getAllSsh($sshTwAPath, $downloadName);
      exit;
    }

    if (isset($_POST["getAllTwB"])) {
      $downloadName = "tw-b-".date('Y-m-d h:i:s', time());
      getAllSsh($sshTwBPath, $downloadName);
      exit;
    }
  }
?>
