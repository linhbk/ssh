<?php
include 'util/admin.php';

 function test($ssh_up_path, $ssh_a_path, $ssh_b_path)
 {
   $myFile = $ssh_a_path;
   $file = file_get_contents($myFile);
   $ssh_up_handle = fopen($ssh_up_path, "r");
   if ($ssh_up_handle) {
       $fh = fopen($myFile, 'a') or die("can't open file");
        while (($ssh_up_line = fgets($ssh_up_handle)) !== false) {
          $ssh_up_ip = explode( '|', $ssh_up_line )[0];
          if(strpos($file, $ssh_up_ip) === FALSE)
          {
             $stringData = $var."\n";
             file_put_contents($ssh_a_path, $ssh_up_line, FILE_APPEND);
             file_put_contents($ssh_b_path, $ssh_up_line, FILE_APPEND);
          }
        }
        fclose($fh);
    }
 }

function filter($ssh_up_path, $ssh_b_path){
   $ssh_up_handle = fopen($ssh_up_path, "r");
   $ssh_b_handle = fopen($ssh_b_path, "r");
   $added_ssh = array();
   if ($ssh_up_handle) {
        while (($ssh_up_line = fgets($ssh_up_handle)) !== false) {
          $ssh_up_ip = explode( '|', $ssh_up_line )[0];
          if (!in_array($ssh_up_ip, $added_ssh)) {
              file_put_contents($ssh_b_path, $ssh_up_line, FILE_APPEND);
              array_push($added_ssh, $ssh_up_ip);
          }
        }
    }
}

function upload($name, $ssh_a_path, $ssh_b_path)
{
     $errors= array();
     $file_name = $_FILES[$name]['name'];
     $file_size = $_FILES[$name]['size'];
     $file_tmp = $_FILES[$name]['tmp_name'];
     $file_type = $_FILES[$name]['type'];
     $file_ext = strtolower(end(explode('.',$_FILES[$name]['name'])));

     $expensions= array("txt");

     if(in_array($file_ext,$expensions)=== false){
        $errors[]="extension not allowed, please choose a TXT file.";
     }

     if($file_size > 2097152) {
        $errors[]='File size must be excately 2 MB';
     }

     if(empty($errors)==true) {
        move_uploaded_file($file_tmp,"uploads/".$file_name);
        echo "Success";
        // filter("uploads/".$file_name, $ssh_b_path);
        test("uploads/".$file_name, $ssh_a_path, $ssh_b_path);
        // foo("uploads/".$file_name, $ssh_a_path, $ssh_b_path);
        chmod("uploads/".$file_name, 0777);
        unlink("uploads/".$file_name);
        header('Location: '.'index.php');
     }else{
        print_r($errors);
     }
}

$ssh_a_us_path = 'ssh_a_us.txt';
$ssh_b_us_path = 'ssh_b_us.txt';
$ssh_a_tw_path = 'ssh_a_tw.txt';
$ssh_b_tw_path = 'ssh_b_tw.txt';

if(isset($_FILES['ssh_us'])){
  upload('ssh_us', $ssh_a_us_path, $ssh_b_us_path);
}

if(isset($_FILES['ssh_tw'])){
  upload('ssh_tw', $ssh_a_tw_path, $ssh_b_tw_path);
}

?>
