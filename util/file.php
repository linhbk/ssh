<?php
  function getLineTotalNumber($filePath){
    $fp = array_filter(array_map("trim", file($filePath)), "strlen");
    return count($fp);
  }
?>
